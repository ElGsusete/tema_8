package teclado_propio;

import java.io.IOException;

public class teclado_integer {

	public static int leerEntero() {
		return Integer.parseInt(leer());
	}
	
	public static String leer() {
		int valor = 0;
		String cadena = "";

		while (valor != 13) {
			try {
				valor = System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
			cadena += ((char) valor);

		}
		return cadena;
	}
	
	public static String leer(String mensaje) {
		System.out.println(mensaje);
		return leer();
	}
	
	
	
	public static void main(String[] argrs) {
		System.out.println("Introduce un numero");
		System.out.println("Numero: " + leerEntero());

	}
}
