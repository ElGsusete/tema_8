package ejercicios_clase;

import java.io.IOException;

public class teclado {

	public static int leerEntero() {
		return Integer.parseInt(leer());
	}

	public static char leerChar() {
		return leer().charAt(0);
	}

	public static boolean leerBoolean() {
		if (leer().equals("true")) {
			return true;
		} 
		else {
			return false;
		}
	}

	public static String leer() {
		int valor = 0;
		String cadena = "";

		while (valor != 13) {
			try {
				valor = System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
			cadena += ((char) valor);

		}
		return cadena;
	}

	public static String leerString() {
		return leer();
	}

	public static void main(String[] argrs) {

		System.out.println(leerBoolean());

	}
}
